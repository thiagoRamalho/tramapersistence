package br.com.trama.persistence.test.mock;

/**
 * @author       $h@rk
 * @Date         07/11/2011
 * @project      TramaPersistence
 * @Description: 
 */
public abstract class MockFactory {
	
	public static Person getPerson(){
		Person person = new Person();
		person.setAge(23);
		person.setName("Test 1");
		return person;
	}
	
	public static Address getAddress(){
		Address address = new Address();
		address.setNumber(10);
		address.setStreet("street");
		return address;
	}
}
