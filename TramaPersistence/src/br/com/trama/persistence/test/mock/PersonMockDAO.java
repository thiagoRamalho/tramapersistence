package br.com.trama.persistence.test.mock;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.trama.persistence.dao.DAO;

/**
 * @author       $h@rk
 * @Date         07/11/2011
 * @project      TramaPersistence
 * @Description: 
 */
public class PersonMockDAO {
	
	private DAO<Person> gd;

	public PersonMockDAO(EntityManager em) {		
		this.gd = new DAO<Person>(em, Person.class);
	}

	public Person save(Person entity) {
		return gd.save(entity);
	}

	public Person findByID(Long id) {
		return gd.findByID(id);
	}

	public List<Person> listAll() {
		return gd.listAll();
	}

	public void remove(Person entity) {
		gd.remove(entity);
	}
	
	public List<Person> findByName(String name){
		return gd.findBy(" where UPPER(e.name) like :pName", "pName", name.toUpperCase());
	}
	
	public Person findByNameAge(Person person){
		
		String where = " where UPPER(e.name) like ?1 ";
		       where+= " and e.age >= ?2 ";
		
		return gd.findBy(where, 1, person.getName().toUpperCase(), 
				                2, person.getAge()).get(0);
	}
		
	public void removeAll(){
		gd.removeAll();
	}

	public List<Person> findByPaging(int firstResult, int maxResults) {		
		return gd.findByPaging(firstResult, maxResults, " ORDER BY e.id ASC ");
	}

	public List<Person> findByPagingDesc(int firstResult, int maxResults) {		
		return gd.findByPaging(firstResult, maxResults, "");
	}

	public List<Person> findByPagingWhereMaxAge(int firstResult, int maxResults, int maxAge) {
		String where = " where e.age < :pAge ";

		return gd.findByPaging(firstResult, maxResults, where, "pAge",  maxAge);
	}

	public Integer rowCount() {
		return gd.rowCount().intValue();
	}
	
	
	
}
