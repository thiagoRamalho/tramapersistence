package br.com.trama.persistence.test.mock;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.trama.persistence.dao.DAO;

/**
 * @author       $h@rk
 * @Date         07/11/2011
 * @project      TramaPersistence
 * @Description: 
 */
public class AddressMockDAO {
	
	private DAO<Address> dao;
	
	public AddressMockDAO(EntityManager em){
		this.dao =  new DAO<Address>(em, Address.class);
	}

	public Address save(Address entity) {
		return dao.save(entity);
	}

	public Address findByID(Long id) {
		return dao.findByID(id);
	}

	public List<Address> listAll() {
		return dao.listAll();
	}

	public void remove(Address entity) {
		dao.remove(entity);
	}
	
	public void removeAll(){
		dao.removeAll();
	}


}
