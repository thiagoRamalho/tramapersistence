package br.com.trama.persistence.test.mock;

import javax.persistence.EntityManager;

import br.com.trama.persistence.util.JPAUtil;

public class JPA {
	
	public EntityManager entityManager;
	public JPAUtil jpaUtil;

	public JPA() {
		this.jpaUtil = new JPAUtil("PU_TRAMA");
	}
	
	public void begin() {	
		entityManager = jpaUtil.getEntityManager();
		entityManager.getTransaction().begin();
	}

	public void commit() {
		entityManager.getTransaction().commit();
		entityManager.close();
	}


}