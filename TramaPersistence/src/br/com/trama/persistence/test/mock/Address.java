package br.com.trama.persistence.test.mock;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * @author       $h@rk
 * @Date         03/11/2011
 * @project      TramaPersistence
 * @Description: 
 */
@Entity
public class Address {
		
	@Id 
	@GeneratedValue
	private Long id;
	private String street;
	private int number;
	
	@ManyToOne(cascade = CascadeType.ALL)
	private Person person;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public Person getPerson() {
		return person;
	}
	public void setPerson(Person person) {
		this.person = person;
	}
}
