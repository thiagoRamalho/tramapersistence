package br.com.trama.persistence.test;


import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import br.com.trama.persistence.test.mock.Address;
import br.com.trama.persistence.test.mock.AddressMockDAO;
import br.com.trama.persistence.test.mock.JPA;
import br.com.trama.persistence.test.mock.MockFactory;
import br.com.trama.persistence.test.mock.Person;
import br.com.trama.persistence.test.mock.PersonMockDAO;


public class RemoveTest {

	JPA data = new JPA();
	
	@Before
	public void setUp() throws Exception {}

	@Test
	public void removePerson(){
		
		PersonMockDAO pmd = null;
		data.begin();
		
		pmd = new PersonMockDAO(data.entityManager);
		AddressMockDAO amd = new AddressMockDAO(data.entityManager);
		
		//remove todos os objetos no banco para garantir teste
		amd.removeAll();
		pmd.removeAll();
		
		Person person = MockFactory.getPerson();
				
		person = pmd.save(person);		
		pmd.remove(person);
		
		assertEquals(0, pmd.listAll().size());
				
		data.commit();		
	}
	
	@Test
	public void removePersonAddress(){
		
		data.begin();
		
		PersonMockDAO  pmd = new PersonMockDAO(data.entityManager);
		AddressMockDAO amd = new AddressMockDAO(data.entityManager);
		
		//remove todos os objetos no banco para garantir teste
		amd.removeAll();
		pmd.removeAll();
				
		Person person = MockFactory.getPerson();
		
		Address address = MockFactory.getAddress();
		
		person = pmd.save(person);
		
		address.setPerson(person);
		
		address = amd.save(address);		
		
		amd.remove(address);
		pmd.remove(person);
		
		assertEquals(0, pmd.listAll().size());
		assertEquals(0, amd.listAll().size());
						
		data.commit();
	}
}
