package br.com.trama.persistence.test;


import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.trama.persistence.test.mock.JPA;
import br.com.trama.persistence.test.mock.MockFactory;
import br.com.trama.persistence.test.mock.Person;
import br.com.trama.persistence.test.mock.PersonMockDAO;

public class PagingTest {

	JPA data = new JPA();
	
	List<Person> persons;
	
	PersonMockDAO pmd;
	
	@Before
	public void setUp() throws Exception {
		data.begin();				
		this.pmd = new PersonMockDAO(data.entityManager);
		
		this.pmd.removeAll();
		
		for (Person p : this.createPersons()) {
			this.pmd.save(p);
		}
		data.commit();
	}
	
	@After
	public void remove(){
		data.begin();
		this.pmd = new PersonMockDAO(data.entityManager);
		this.pmd.removeAll();
		data.commit();
	}
	
	@Test
	public void findByPaging3Results(){
		data.begin();
		this.pmd = new PersonMockDAO(data.entityManager);
		List<Person> list = this.pmd.findByPaging(0, 3);
		
		assertEquals(3, list.size());
		System.out.println("---------------------------");
		for(int i = 0; i < list.size(); i++){
			
			String name = list.get(i).getName();
			
			System.out.println(name);
			
			assertEquals("Test "+i, name);
		}
		
		data.commit();
		System.out.println("---------------------------");
	}
	
	@Test
	public void findByPagingOrderBy(){
		data.begin();
		this.pmd = new PersonMockDAO(data.entityManager);
		List<Person> list = this.pmd.findByPagingDesc(0, 3);
		
		assertEquals(3, list.size());
		System.out.println("---------------------------");
		for(int i = 0; i < list.size(); i++){
			
			String name = list.get(i).getName();
			Long id = list.get(i).getId();
			
			System.out.println(id+" - "+name);
		}
		
		data.commit();
		System.out.println("---------------------------");
	}
	
	@Test
	public void findByPaging5Results(){
		data.begin();
		this.pmd = new PersonMockDAO(data.entityManager);
		List<Person> list = this.pmd.findByPaging(3, 5);
		
		assertEquals(5, list.size());
		System.out.println("---------------------------");
		for(int i = 0; i < list.size(); i++){
			
			String name = list.get(i).getName();
			
			System.out.println(name);
			
			assertEquals("Test "+(i+3), name);
		}
		
		data.commit();
		System.out.println("---------------------------");
	}
	
	@Test
	public void findByPaging5ResultsNoLinear(){
		data.begin();
		this.pmd = new PersonMockDAO(data.entityManager);
		
		Person p = this.pmd.findByName("Test 4").get(0);		
		this.pmd.remove(p);
		
		List<Person> list = this.pmd.findByPaging(2, 5);
		
		assertEquals(5, list.size());
		System.out.println("---------------------------");
		for(int i = 0; i < list.size(); i++){
			
			String name = list.get(i).getName();
			
			String expected = "Test "+(2+i);
			
			//ajusta o indice pois o 4 elemento
			//foi eliminado
			if(i > 1){
				expected = "Test "+(3+i);
			}
			
			System.out.println("Expected: "+expected+" - Result: "+name);
			
			assertEquals(expected, name);
		}
		
		data.commit();
		System.out.println("---------------------------");
	}
	
	@Test
	public void findByPaging10ResultsNoLinear(){
		data.begin();
		this.pmd = new PersonMockDAO(data.entityManager);
		
		Person p = this.pmd.findByName("Test 4").get(0);		
		this.pmd.remove(p);
		
		Person newP = this.pmd.findByName("Test 0").get(0);		
		this.pmd.remove(newP);
		
		List<Person> list = this.pmd.findByPaging(1, 10);
		
		assertEquals(10, list.size());
		System.out.println("---------------------------");
		for(int i = 0; i < list.size(); i++){
			
			String name = list.get(i).getName();
			
			String expected = "Test "+(2+i);
			
			//ajusta o indice pois o 4 elemento
			//foi eliminado
			if(i > 1){
				expected = "Test "+(3+i);
			}			
			
			System.out.println("Expected: "+expected+" - Result: "+name);
			
			assertEquals(expected, name);
		}
		
		data.commit();
		System.out.println("---------------------------");
	}
	
	@Test
	public void findByPagingWithWhereMaxAge(){
		data.begin();
		this.pmd = new PersonMockDAO(data.entityManager);
		
		//comeca no indice zero e pode retornar ate 30 registros
		//desde que com a idade menor que 20 anos
		List<Person> list = this.pmd.findByPagingWhereMaxAge(0, 30, 20);
		
		assertEquals(20, list.size());
		System.out.println("---------------------------");
		for(int i = 0; i < list.size(); i++){
			
			int age = list.get(i).getAge();
						
			System.out.println("Expected Age:  "+(i)+" - Result Age: "+age);
			
			assertEquals(i, age);
		}
		
		data.commit();
		System.out.println("---------------------------");
	}

	
	@Test
	public void count(){
		data.begin();
		this.pmd = new PersonMockDAO(data.entityManager);
		
		assertEquals(new Integer(100), this.pmd.rowCount());
				
		data.commit();
	}
	
	private List<Person> createPersons(){
		
		List<Person> persons = new ArrayList<Person>();
		
		for(int i = 0; i < 100; i++){
			Person p = MockFactory.getPerson();
			p.setName("Test "+i);
			p.setAge(i);
			persons.add(p);
		}
		
		return persons;
	}
}
