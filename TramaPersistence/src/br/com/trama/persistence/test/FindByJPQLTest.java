package br.com.trama.persistence.test;


import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import br.com.trama.persistence.test.mock.JPA;
import br.com.trama.persistence.test.mock.MockFactory;
import br.com.trama.persistence.test.mock.Person;
import br.com.trama.persistence.test.mock.PersonMockDAO;

public class FindByJPQLTest {

	JPA data = new JPA();
	
	@Before
	public void setUp() throws Exception {
		data.begin();
		
		PersonMockDAO pmd = new PersonMockDAO(data.entityManager);
		
		pmd.removeAll();
		
		data.commit();
	}
	
	@Test
	public void findByID(){

		data.begin();
		Person person = MockFactory.getPerson();
		
		PersonMockDAO pmd = new PersonMockDAO(data.entityManager);

		//salva o objeto para testar sua recuperacao
		person = pmd.save(person);		
		
		Person p = pmd.findByID(person.getId());
		
		assertEquals(23, p.getAge());
		assertEquals("Test 1", p.getName());
		
		data.commit();	
	}

	@Test
	public void findByName(){

		data.begin();
		Person person = MockFactory.getPerson();
		
		PersonMockDAO pmd = new PersonMockDAO(data.entityManager);
				
		person = pmd.save(person);		
		
		Person p = pmd.findByName(person.getName().toLowerCase()).get(0);
		
		assertEquals(23, p.getAge());
		assertEquals("Test 1", p.getName());
		
		data.commit();	
	}

	@Test
	public void findByNameAge(){

		data.begin();
		Person person = MockFactory.getPerson();
		
		PersonMockDAO pmd = new PersonMockDAO(data.entityManager);
				
		person = pmd.save(person);	
		
		Person person2 = MockFactory.getPerson();
		
		person2.setAge(33);
		
		person2 = pmd.save(person2);
		
		Person p = pmd.findByNameAge(person2);
		
		assertEquals(33, p.getAge());
		assertEquals("Test 1", p.getName());
		
		data.commit();	
	}

}
