package br.com.trama.persistence.test;


import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

import br.com.trama.persistence.util.JPAUtil;


public class CreateEntityManagerFactoryTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void createEntityManagerFactory(){
		JPAUtil jpaUtil = new JPAUtil("PU_TRAMA");
		assertNotNull(jpaUtil.getEntityManager());
	}
}
