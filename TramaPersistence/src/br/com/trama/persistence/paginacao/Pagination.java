package br.com.trama.persistence.paginacao;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe utilitaria para controle de paginacao
 * @author Thiago Ramalho
 *
 * @param <T>
 */
public class Pagination<T> {
        
        private Integer actual;
        private Integer totalPage;
        private Integer maxPerPage;
        private Integer totalRecords;
        private Integer currentPage;
        private List<T> list;
        
        /**
         * Configura objeto com os elementos para iteracao das listas
         * @param maxPerPage
         * @param totalRecords
         */
        public Pagination(Integer maxPerPage, Integer totalRecords) {
                this.maxPerPage = maxPerPage;
                this.totalRecords = totalRecords;
                this.list = new ArrayList<T>();
        }
        
        /**
         * Realiza o calculo para determinar os valores de pagina atual e maximo por pagina
         * @param current
         * @param totalPerPage
         */
        public void execute(Integer atual) {
                
                //pagina acessada
                this.currentPage = atual == null ? 1 : atual.intValue();
                
                //valor inicial do range de registros
                this.actual     = (this.maxPerPage * this.currentPage) - this.maxPerPage;
                
                //total de paginas que devem ser criadas
                this.totalPage  = new Double(Math.ceil((double) this.totalRecords / this.maxPerPage)).intValue();
        }       
        
        public Integer getActual() {
                return actual;
        }

        public List<T> getList() {
                return list;
        }

        public void setList(List<T> list) {
                this.list = list;
        }

        public Integer getTotalPage() {
                return totalPage;
        }

        public Integer getCurrentPage() {
                return currentPage;
        }       
}