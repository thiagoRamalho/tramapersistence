package br.com.trama.persistence.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * 
 * @author Thiago Ramalho
 *
 */
public class JPAUtil {	
		
	private String persistenceUnit;
		
	private static EntityManagerFactory entityManagerFactory;
	
	public JPAUtil(String persistenUnit) {
		this.persistenceUnit = persistenUnit;
		this.configEMF();
	}
	
	/**
	 * utiliza essa implementacao para que ao iniciar a classe
	 *ja seja carregado a factory com base na persitenceUnit
	*setada
	*/
	private void configEMF() {
		if(entityManagerFactory == null){
		   entityManagerFactory = 
		   Persistence.createEntityManagerFactory(persistenceUnit);
		}
	}

	/**
	 * Retorna uma instancia de entityManager
	 * para utilizacao na persistencia dos dados
	 * 
	 * @return
	 */
	public EntityManager getEntityManager(){
		return entityManagerFactory.createEntityManager();
	}
}
