package br.com.trama.persistence.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;


/**
 * 
 * @author Thiago Ramalho
 *
 * @param <T>
 */
public class DAO<T> {

	private EntityManager entityManager;
	private Class<T> classe;
	
	/**
	 * Injetar Objetos para que seja possivel trabalhar
	 * com metodos de manipulacao de registros no BD
	 * 
	 * @param entityManager
	 * @param classe
	 */
	public DAO(EntityManager entityManager, Class<T> classe) {
		this.entityManager = entityManager;
		this.classe = classe;
	}
	
	/**
	 * Salva entidade e retorna a mesma em estado
	 * managed
	 * 
	 * @param entity
	 * @return
	 */
	public final T save(T entity){
		return this.entityManager.merge(entity);
		
	}
	
	/**
	 * Lista uma determinada entidade tendo
	 * seu id como filtro
	 * 
	 * @param id
	 * @return
	 */
	public final T findByID(Long id){
		
		if(id != null){			
			return this.entityManager.getReference(this.classe, id);
		}
		
		return null;		
	}
	
	/**
	 * Lista todos os registros de determinada entidade
	 * caso nao encontre registros retorna lista vazia
	 * 
	 * @return
	 */
	public final List<T> listAll(){
		return this.findByJPQL(this.configQueryWithEntity());
	}
	
	/**
	 * Remove entidade
	 * 
	 * @param entity
	 * @return 
	 */
	public final void remove(T entity){
		this.entityManager.remove(entity);
	}
	
	/**
	 * Realiza busca generica 
	 * @param query query completa com restricoes
	 * @param params parametros das restricoes
	 * @return
	 */
	public final List<T> findByJPQL(String query, Object... params){
		
		List<T> list = null;
		
		TypedQuery<T>  q = null;
		
		try {		
			 
			q = this.entityManager.createQuery(query, this.classe);
			
			q= addParameter(q, params);
			
			return  q.getResultList();
			
		} catch (NoResultException e) {
			list = new ArrayList<T>();
		}
		
		return list;
	}
	
	/**
	 * Busca generica retornando lista com entidade 
	 * que satisfaca os parametros de with
	 * 
	 * @param where   restricoees
	 * @param params variavel que possui o valor e valor de
	 *               restricao
	 * @return
	 */
	public final List<T> findBy(String where, Object... params){
		String jpql = this.configQueryWithEntity().concat(where);
		return this.findByJPQL(jpql, params);
	}
	
	/**
	 * Realiza concatenacoes para criacao da
	 * query de retorno
	 * 
	 * @return
	 */
	public String configQueryWithEntity(){
		StringBuilder query = new StringBuilder("");
		query.append(" select e from ");
		query.append(this.classe.getName());
		query.append(" e ");
		
		return query.toString();
	}
	
	/**
	 * Remove todos os registros de T
	 */
	public void removeAll() {
		entityManager.createQuery("DELETE FROM "+this.classe.getName()).executeUpdate();		
	}
	
	/**
	 * Conta quantidade de registros da entidade
	 * 
	 * @return
	 */
	public Long rowCount(){
		
		String query = "select count(*) from "+this.classe.getName();
		
		TypedQuery<Long>  q = this.entityManager.createQuery(query, Long.class);
		
		return q.getSingleResult();	       
	}
	
	/**
	 * Busca paginada
	 * @param query
	 * @param firstResult
	 * @param maxResults
	 * @return
	 */
	public List<T> findByPaging(int firstResult, int maxResults, String where, Object... params){

		List<T> list = new ArrayList<T>();

		TypedQuery<T>  q = null;

		try {		
						
			if(where.isEmpty()){
				where = where.concat("ORDER BY e.id desc");
			}        
			
			String query = this.configQueryWithEntity().concat(where);
			
			q = this.entityManager.createQuery(query, this.classe);
			
			q = this.addParameter(q, params);
			
			q.setFirstResult(firstResult);
			q.setMaxResults(maxResults);

			list = q.getResultList();

		} catch (NoResultException e) {
			list = new ArrayList<T>();
		}

		return list;
	}
	
	/**
	 * Adiciona parametros a query
	 * 
	 * @param q
	 * @param params
	 * @return
	 */
	private TypedQuery<T> addParameter(TypedQuery<T> q, Object... params) {
		for (int i = 0; i < params.length; i++) {				
			q.setParameter(params[i].toString(), params[++i]);
		}
		
		return q;
	}

}
